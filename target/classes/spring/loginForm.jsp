<%@ include file="/WEB-INF/jsp/common/includes.jsp"%>
<html>
<head>
<title>Login Page</title>
</head>
<body onload='document.f.j_username.focus();'>
	<h3>Login with Username and Password</h3>
	<div class="container">
		<h1>Login</h1>
		<div class="span-12 last">
			<form name='f' action='j_spring_security_check' method='POST'>
				<fieldset>
					<legend></legend>
					<p>
						<labelcssErrorClass="error">User<label>
							<br /> <input type='text' name='j_username' value=''>
					</p>
					<p>
						<label cssErrorClass="error">Password</label> <br /> <input
							type='password' name='j_password' />
					</p>
					<p>
						<input type="submit" value="Login" />
					</p>
				</fieldset>
			</form>
		</div>
		<hr />
	</div>
</body>
</html>