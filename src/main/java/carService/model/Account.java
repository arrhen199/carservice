package carService.model;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

@Entity
public class Account {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String username;
	@Email
	private String email;
	@Size(min = 3, max = 10, message = "{user.password.validation.Size.message}")
	private String password;

	private Set<Car> cars;

	/*@OneToMany(mappedBy = "account", cascade = CascadeType.ALL)*/
	public Set<Car> getCars() {
		return cars;
	}

	public void setCars(Set<Car> cars) {
		this.cars = cars;
	}

	@Override
	public String toString() {

		StringBuilder res = new StringBuilder();
		res.append("User [id=" + id + ", username=" + username + ", email=" + email + ", password=" + password
				+ ", cars=" + cars + "]");
		if (cars != null) {
			for (Car car : cars) {
				/*
				 * res += String.format( "Car[mark=%d, model='%s']%n",
				 * car.getMark(), car.getModel());
				 */
			}
		}
		return res.toString();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
