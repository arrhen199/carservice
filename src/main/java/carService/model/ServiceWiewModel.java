package carService.model;

import java.util.List;

import carService.model.Car;
import carService.model.Mechanic;

public class ServiceWiewModel {
	
	private Integer id;	
	private String price;
	private String serviceDate;
	private String description;
	private Car car;	

	private List<Mechanic> mechanics;

	private int[] selectedMechanics;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getServiceDate() {
		return serviceDate;
	}

	public void setServiceDate(String serviceDate) {
		this.serviceDate = serviceDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public List<Mechanic> getMechanics() {
		return mechanics;
	}

	public void setMechanics(List<Mechanic> mechanics) {
		this.mechanics = mechanics;
	}

	public int[] getSelectedMechanics() {
		return selectedMechanics;
	}

	public void setSelectedMechanics(int[] selectedMechanics) {
		this.selectedMechanics = selectedMechanics;
	}

	
	
}
