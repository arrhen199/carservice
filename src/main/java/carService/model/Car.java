package carService.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
public class Car {		
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String mileage;
	private String model;
	private String mark;
	@ManyToOne
	@JoinColumn( referencedColumnName="id")
	private Account account;
	//private Set<Service> services;

	public Account getUser() {
		return account;
	}
	public void setUser(Account user) {
		this.account = user;
	}	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMileage() {
		return mileage;
	}
	public void setMileage(String mileage) {
		this.mileage = mileage;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getMark() {
		return mark;
	}
	public void setMark(String mark) {
		this.mark = mark;
	}
	/*public Set<Service> getServices() {
		return services;
	}
	public void setServices(Set<Service> services) {
		this.services = services;
	}
	@Override
	public String toString() {
		return "Car [id=" + id + ", mileage=" + mileage + ", model=" + model + ", mark=" + mark + ", services="
				+ services + "]";
	}*/

}
