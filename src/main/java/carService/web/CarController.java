package carService.web;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import carService.service.CarService;
import carService.service.IUserService;
import carService.service.UserService;
import carService.model.Account;
import carService.model.Car;
import carService.model.Mechanic;

@Controller
@RequestMapping(value = "/car")
@SessionAttributes(types = Car.class)
public class CarController {

	@Autowired
	private CarService carService;
	@Autowired
	private UserService userService;

	@RequestMapping(value = "list")
	public String carList(Model model) {
		List<Car> cars = carService.getCars();
		model.addAttribute("cars", cars);
		userService.getAll();
		return "carsList";
	}

	@RequestMapping(value = "add", method = RequestMethod.GET)
	public String addCar(Model model) {
		model.addAttribute("car", new Car());
		return "carForm";
	}
	


	@RequestMapping(value = "add", method = RequestMethod.POST)
	public String addCar(@Valid @ModelAttribute("car") Car car, BindingResult result, SessionStatus status) {

		if (result.hasErrors()) {
			return "carForm";
		}

		Account user = userService.getUser();
		car.setUser(user);
		status.setComplete();
		carService.save(car);
		return "redirect:/car/list";
	}

	@RequestMapping(value = "edit/{carId}", method = RequestMethod.GET)
	public String editCar(@PathVariable("carId") int id, Model model) {
		Car car = carService.getCarById(id);
		model.addAttribute("car", car);
		return "carForm";
	}

	@RequestMapping(value = "delete/{carId}", method = RequestMethod.GET)
	public String deleteCar(@PathVariable("carId") int id, Model model) {
		Car car = carService.getCarById(id);
		carService.delete(car);
		return "redirect:/car/list";
	}

	@RequestMapping(value = "edit/{id}", method = RequestMethod.POST)
	public String editCar(@Valid @ModelAttribute("car") Car car, BindingResult result, SessionStatus status) {
		if (result.hasErrors()) {
			return "carForm";
		}
		status.setComplete();
		carService.save(car);
		return "redirect:/service/list/" + car.getId();
	}

	@RequestMapping(value = "update", method = RequestMethod.POST)
	public String updateCar(@Valid @ModelAttribute("car") Car car, BindingResult result, SessionStatus status) {

		if (result.hasErrors()) {
			return "productForm";
		}

		Account user = userService.getUser();
		car.setUser(user);
		status.setComplete();
		carService.save(car);
		return "redirect:/car/list";
	}
}
