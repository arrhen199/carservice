package carService.web;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import carService.service.CarService;
import carService.model.Account;
import carService.model.Car;
import carService.model.Mechanic;
import carService.model.MechanicService;
import carService.model.Service;
import carService.model.ServiceWiewModel;

@Controller
@RequestMapping(value = "/service")
public class ServiceController {

	@Autowired
	private CarService carService;

	@RequestMapping(value = "list/{id}")
	public String serviceList(@PathVariable("id") int id, Model model) {
		Car car = carService.getCarById(id);
		List<Service> services = carService.getServicesByCarId(id);
		model.addAttribute("services", services);
		model.addAttribute("car", car);
		return "serviceList";
	}

	@RequestMapping(value = "{id}")
	public String serviceView(@PathVariable("id") int id, Model model) {
		Service service = carService.getService(id);
		List<Mechanic> mechanics = carService.getMechanicsByServiceId(id);
		model.addAttribute("service", service);
		model.addAttribute("mechanics", mechanics);
		return "serviceView";
	}

	@RequestMapping(value = "add/{id}", method = RequestMethod.GET)
	public String addService(@PathVariable("id") int id, Model model) {
		ServiceWiewModel service = new ServiceWiewModel();
		service.setCar(carService.getCarById(id));
		service.setMechanics(carService.getMechanics());
		model.addAttribute("service", service);
		return "serviceForm";
	}

	@RequestMapping(value = "add/{id}", method = RequestMethod.POST)
	public String addService(@PathVariable("id") int id,@Valid @ModelAttribute("service") ServiceWiewModel service, BindingResult result, SessionStatus status) {

		if (result.hasErrors()) {
			return "serviceForm";
		}
		
		Service dbService = new Service();
		Car car = carService.getCarById(id);
		int[] mechIds = service.getSelectedMechanics();
		
		dbService.setCar(car);
		dbService.setDescription(service.getDescription());
		dbService.setPrice(service.getPrice());
		dbService.setServiceDate(service.getServiceDate());
		
		dbService = carService.save(dbService);
		
		
		for(int mechanicId : mechIds){
			MechanicService tmp = new MechanicService();
			tmp.setService(dbService);
			tmp.setMechanic(carService.getMechanic(mechanicId));
			carService.save(tmp);
		}		
		
	
		return "redirect:/service/list/"+car.getId();
	}
	
	@RequestMapping(value = "mechanic/add", method = RequestMethod.GET)
	public String addMechanic(Model model) {
		Mechanic m = new Mechanic();
		m.setBirthYear(1990);
		model.addAttribute("mechanic", m);
		return "mechanicForm";
	}
	@RequestMapping(value = "mechanic/add", method = RequestMethod.POST)
	public String addMechanic(@Valid @ModelAttribute("mechanic") Mechanic mechanic, BindingResult result, SessionStatus status) {
		if (result.hasErrors()) {
			return "mechanicForm";
		}
		status.setComplete();
		carService.save(mechanic);
		return "redirect:/service/mechanic/list";
	}
	
	@RequestMapping(value = "mechanic/list", method = RequestMethod.GET)
	public String mechanicList(Model model) {
		model.addAttribute("mechanics", carService.getMechanics());
		return "mechanicList";
	}

}
