package carService.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import carService.service.IUserService;
import carService.service.UserService;
import carService.model.Account;

@Controller
@RequestMapping(value = "/user")
@SessionAttributes(types = Account.class)
public class UserController {
	
	@Autowired
	private IUserService userService;
	
	@RequestMapping(value = "login", method = RequestMethod.GET)
	public String login(Model model) {		
		model.addAttribute("user", new Account());
		return "loginForm";
	}
	
	@RequestMapping(value = "logout", method = RequestMethod.GET)
	public String logout(HttpServletRequest request, HttpServletResponse response) {		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    if (auth != null){    
	        new SecurityContextLogoutHandler().logout(request, response, auth);
	    }
	    return "redirect:/login";
	}
	
	@RequestMapping(value = "login", method = RequestMethod.POST)
	public String add(@Valid @ModelAttribute ("user") Account user, BindingResult result, SessionStatus status) {		
				
		if (result.hasErrors()) {
			return "loginForm";
		}
		
		status.setComplete();
		return "redirect:/user";
	}

}
