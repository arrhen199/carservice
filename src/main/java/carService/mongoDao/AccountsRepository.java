package carService.mongoDao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import carService.mongoModel.Account;

@Repository
public interface AccountsRepository extends MongoRepository<Account, String> {

}
