package carService.dao;

import org.springframework.data.repository.CrudRepository;

import carService.model.Account;

public interface AccountDao extends CrudRepository<Account, Integer> {

	Account findByUsername(String username);

}
