package carService.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import carService.model.Car;
import carService.model.Service;

public interface ServiceDao extends CrudRepository<Service, Integer> {

	List<Service> findAll();
	List<Service> findByCarId(int carId);
	Service findById(int id);

}