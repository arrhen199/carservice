package carService.dao;

import java.util.List;
import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import carService.model.Car;

public interface CarDao extends CrudRepository<Car, Integer> {

	List<Car> findAll();
	List<Car> findByAccountId(int account_id);
	Car findById(int id);

}