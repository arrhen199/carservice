package carService.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import carService.model.Mechanic;

public interface MechanicDao extends CrudRepository<Mechanic, Integer> {

	List<Mechanic> findAll();
	Mechanic findById(int id);

}
