package carService.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import carService.model.MechanicService;

public interface MechanicServiceDao extends CrudRepository<MechanicService, Integer> {

	List<MechanicService> findAll();
	List<MechanicService> findByServiceId(int service_id);
	List<MechanicService> findByMechanicId(int mechanic_id);
	MechanicService findById(int id);

}