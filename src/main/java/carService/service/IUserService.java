package carService.service;

import org.springframework.security.core.Authentication;

import carService.model.Account;

public interface IUserService {
	 String getUsername();
	 Account getUser();
	 Authentication getAuthentication();
	 void getAll();
}
