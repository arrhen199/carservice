package carService.service;

import java.util.List;
import java.util.Set;

import carService.model.Car;
import carService.model.Mechanic;
import carService.model.MechanicService;
import carService.model.Service;

public interface CarService {
	Car getCarById(int id);
	List<Car> getCarByUserId(int id);
	List<Car> getCars();
	void save(Car car);
	void delete(Car car);
	
	List<Service> getServices();
	Service getService(int id);
	List<Service> getServicesByCarId(int carId);
	Service save(Service service);
	
	Mechanic getMechanic(int id);	
	List<Mechanic> getMechanics();
	void save(Mechanic mechanic);
	
	List<Mechanic> getMechanicsByServiceId(int service_id);
	void save(MechanicService mechanicService);
	
	
}
