package carService.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import carService.dao.CarDao;
import carService.dao.MechanicDao;
import carService.dao.MechanicServiceDao;
import carService.dao.ServiceDao;
import carService.model.Car;
import carService.model.Mechanic;
import carService.model.MechanicService;

@Service
public class CarServiceImpl implements CarService {

	@Autowired
	private CarDao carDao;
	@Autowired
	private ServiceDao serviceDao;
	@Autowired
	private MechanicDao mechanicDao;
	@Autowired
	private MechanicServiceDao mechanicServiceDao;

	public Car getCarById(int id) {
		return carDao.findById(id);
	}

	public List<Car> getCarByUserId(int id) {
		return carDao.findByAccountId(id);
	}

	public List<Car> getCars() {
		return carDao.findAll();
	}

	public List<carService.model.Service> getServices() {
		return serviceDao.findAll();
	}

	public List<carService.model.Service> getServicesByCarId(int carId) {
		return serviceDao.findByCarId(carId);
	}

	public void save(Car car) {
		carDao.save(car);
	}

	public void delete(Car car) {
		carDao.delete(car);

	}

	public carService.model.Service getService(int id) {
		return serviceDao.findById(id);
	}

	public Mechanic getMechanic(int id) {
		return mechanicDao.findById(id);
	}

	public List<Mechanic> getMechanicsByServiceId(int service_id) {

		List<MechanicService> tmp = mechanicServiceDao.findByServiceId(service_id);
		List<Mechanic> res = new ArrayList<Mechanic>();
		for (MechanicService mechSer : tmp) {
			res.add(mechSer.getMechanic());
		}

		return res;
	}

	public List<Mechanic> getMechanics() {
		return mechanicDao.findAll();
	}

	public carService.model.Service save(carService.model.Service service) {
		return serviceDao.save(service);
	}

	public void save(Mechanic mechanic) {
		mechanicDao.save(mechanic);
	}

	public void save(MechanicService mechanicService) {
		mechanicServiceDao.save(mechanicService);
	}

}
