package carService.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import carService.dao.AccountDao;
import carService.model.Account;
import carService.mongoDao.AccountsRepository;

@Service
public class UserService implements IUserService {
	@Autowired
	private AccountDao userDao;
	@Autowired
	private AccountsRepository repository;
	private Account user;

	public String getUsername() {
		UserDetails principal = (UserDetails) getAuthentication().getPrincipal();
		return principal.getUsername();
	}

	public Account getUser() {
		if (user == null) {
			Account a = userDao.findByUsername(getUsername());
			if (a == null)
				return null;
			user = a;
		}
		return user;
	}
	public void getAll(){
		List<carService.mongoModel.Account> users = repository.findAll();
		for(int i = 0; i < users.size();i++){
			System.out.println(users.get(i).getUsername());
		}
	}

	public Authentication getAuthentication() {
		Authentication a = SecurityContextHolder.getContext().getAuthentication();
		if (a == null) {
			//a = (Authentication) ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
			//		.getRequest()).getSession(true).getAttribute("SPRING_SECURITY_CONTEXT");
		}
		return a;
	}
}
