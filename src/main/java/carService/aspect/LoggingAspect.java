package carService.aspect;

import java.util.concurrent.TimeUnit;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class LoggingAspect {

	@Around("execution(* carService.web.*.* (..))")
	public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {

		long time = System.currentTimeMillis();
		//System.out.println("logAround() is running!");
		System.out.println("hijacked class : " + joinPoint.getTarget().getClass());
		System.out.println("hijacked method : " + joinPoint.getSignature().getName());
		//System.out.println("Around before is running!");
		Object result = joinPoint.proceed(); // continue on the intercepted method
		//System.out.println("Around after is running!");
		System.out.println("		Execution Time : " + (System.currentTimeMillis() - time) + " milisec");
		System.out.println("******");
		return result;

	}

}
