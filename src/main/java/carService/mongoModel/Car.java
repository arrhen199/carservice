package carService.mongoModel;

import javax.persistence.Id;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "cars")
public class Car {		
	@Id
	private Integer id;
	private String mileage;
	private String model;
	private String mark;
	//private Set<Service> services;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMileage() {
		return mileage;
	}
	public void setMileage(String mileage) {
		this.mileage = mileage;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getMark() {
		return mark;
	}
	public void setMark(String mark) {
		this.mark = mark;
	}
	/*public Set<Service> getServices() {
		return services;
	}
	public void setServices(Set<Service> services) {
		this.services = services;
	}
	@Override
	public String toString() {
		return "Car [id=" + id + ", mileage=" + mileage + ", model=" + model + ", mark=" + mark + ", services="
				+ services + "]";
	}*/

}