<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<html>
<head>
<META  http-equiv="Content-Type"  content="text/html;charset=UTF-8">
<title>Cer Service</title>
<link rel="stylesheet"
	href="<c:url value="/resources/blueprint/screen.css" />"
	type="text/css" media="screen, projection">
<link rel="stylesheet"
	href="<c:url value="/resources/blueprint/print.css" />" type="text/css"
	media="print">
<!--[if lt IE 8]>
		<link rel="stylesheet" href="<c:url value="/resources/blueprint/ie.css" />" type="text/css" media="screen, projection">
	<![endif]-->
</head>
<body>
	<div class="container">
		<div class="span-12 last">
			<table>

				<td><a href="<c:url value='/service/mechanic/list'/>">Mechanics
						List</a></td>
				<td><a href="<c:url value='/car/list'/>">Cars List</a></td>
				<td><a href="<c:url value='/user/logout'/>">Logout</a></td>

			</table>
		</div>
	</div>