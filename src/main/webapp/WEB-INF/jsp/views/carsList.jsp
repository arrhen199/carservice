<%@ include file="/WEB-INF/jsp/common/includes.jsp" %>
<%@ include file="/WEB-INF/jsp/common/header.jsp" %>
<div class="container"> 
	<div class="span-12 last">
		<h1>Car list</h1>	
		<a href="<c:url value='/car/add'/>">add</a>
		<table>
		  	<thead>
			   <th>Id</th>
			   <th>Mark</th>
			   <th>Model</th>    
		  </thead>
		  <c:forEach var="car" items="${cars}">
		    <tr onclick="document.location='../service/list/${car.id}';">  
		      <td>${car.id}</td>
		      <td>${car.mark}</td>
		      <td>${car.model}</td>      
		    </tr>
		  </c:forEach>
		</table>	
	</div>
	<hr/>
	<a href="<c:url value='/'/>">back</a>	
</div>
<%@ include file="/WEB-INF/jsp/common/footer.jsp" %>