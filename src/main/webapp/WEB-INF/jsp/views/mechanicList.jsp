<%@ include file="/WEB-INF/jsp/common/includes.jsp" %>
<%@ include file="/WEB-INF/jsp/common/header.jsp" %>
<div class="container"> 
	<div class="span-12 last">
		<h1>Mechanics </h1>	
		<a href="<c:url value='/service/mechanic/add'/>">add</a>
		<table>
		  	<thead>
			   <th>Name</th>
			   <th>Year of birth</th>   
		  </thead>
		  <c:forEach var="mechanic" items="${mechanics}">
		      <td>${mechanic.name}</td>
		      <td>${mechanic.birthYear}</td>      
		    </tr>
		  </c:forEach>
		</table>	
	</div>
	<hr/>
	<a href="<c:url value='/'/>">back</a>	
</div>
<%@ include file="/WEB-INF/jsp/common/footer.jsp" %>