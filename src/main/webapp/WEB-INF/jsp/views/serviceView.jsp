<%@ include file="/WEB-INF/jsp/common/includes.jsp"%>
<%@ include file="/WEB-INF/jsp/common/header.jsp"%>
<div class="container">
	<div class="span-12 last">
		<h1>${service.serviceDate}</h1>
		<h2>${service.description}</h2>
		<h3>${service.price}</h3>
		<table>
			<thead>
				<th>Mechanics</th>
			</thead>
			<c:forEach var="mechanic" items="${mechanics}">
				<tr >
					<td>${mechanic.name}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
	<hr />
	<a href="<c:url value='/service/list/${service.car.id }'/>">back</a>
</div>
<%@ include file="/WEB-INF/jsp/common/footer.jsp"%>