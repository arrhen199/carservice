<%@ include file="/WEB-INF/jsp/common/includes.jsp"%>
<%@ include file="/WEB-INF/jsp/common/header.jsp"%>
<div class="container">
	<h1>Car form</h1>
	<div class="span-12 last">
		<form:form modelAttribute="car" method="post">
			<fieldset>
				<legend>Car fields</legend>
				<p>
					<form:label for="mark" path="mark" cssErrorClass="error">Mark</form:label>
					<br />
					<form:input path="mark" />
					<form:errors path="mark" />
				</p>
				<p>
					<form:label for="model" path="model" cssErrorClass="error">Model</form:label>
					<br />
					<form:input path="model" />
					<form:errors path="model" />
				</p>
				<p>
					<form:label for="mileage" path="mileage" cssErrorClass="error">Mileage</form:label>
					<br />
					<form:input path="mileage" />
					<form:errors path="mileage" />
				</p>
				<p>
					<input type="submit" value="Save" />
				</p>
			</fieldset>
		</form:form>
	</div>
	<hr />
	<a href="<c:url value='/car/list'/>">back</a>
</div>

<%@ include file="/WEB-INF/jsp/common/footer.jsp"%>