<%@ include file="/WEB-INF/jsp/common/includes.jsp"%>
<%@ include file="/WEB-INF/jsp/common/header.jsp"%>
<div class="container">
	<h1>Mechanic form</h1>
	<div class="span-12 last">
		<form:form modelAttribute="mechanic" action="../../service/mechanic/add" method="post">
			<fieldset>
				<legend>Car fields</legend>
				<p>
					<form:label for="name" path="name" cssErrorClass="error">Name</form:label>
					<br />
					<form:input path="name" />
					<form:errors path="name" />
				</p>
				<p>
					<form:label for="birthYear" path="birthYear" cssErrorClass="error">Year of birth</form:label>
					<br />
					<form:input path="birthYear" />
					<form:errors path="birthYear" />
				</p>
				<p>
					<input type="submit" value="Save" />
				</p>
			</fieldset>
		</form:form>
	</div>
	<hr />
	<a href="<c:url value='/car/list'/>">back</a>
</div>

<%@ include file="/WEB-INF/jsp/common/footer.jsp"%>