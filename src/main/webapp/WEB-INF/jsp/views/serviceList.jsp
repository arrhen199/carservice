<%@ include file="/WEB-INF/jsp/common/includes.jsp"%>
<%@ include file="/WEB-INF/jsp/common/header.jsp"%>
<div class="container">
	<div class="span-12 last">
		<h1>${car.mark}</h1>
		<h2>${car.model}</h2>
		<h3>${car.mileage}</h3>
		<table>
			<tr>
				<td><a href="<c:url value='/car/edit/${car.id}'/>">edit</a></td>
				<td><a href="<c:url value='/car/delete/${car.id}'/>">delete</a></td>

			</tr>
		</table>
		<table>
			<thead>
				<th>Date</th>
				<th>Description</th>
				<th>Price</th>
			</thead>
			<c:forEach var="service" items="${services}">
				<tr onclick="document.location='../../service/${service.id}';">
					<td>${service.serviceDate}</td>
					<td>${service.description}</td>
					<td>${service.price}</td>

				</tr>
			</c:forEach>
		</table>
		<a href="<c:url value='/service/add/${ car.id }'/>">add</a>
	</div>
	<hr />
	<a href="<c:url value='/car/list'/>">back</a>
</div>
<%@ include file="/WEB-INF/jsp/common/footer.jsp"%>