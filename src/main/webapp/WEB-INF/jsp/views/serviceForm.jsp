<%@ include file="/WEB-INF/jsp/common/includes.jsp"%>
<%@ include file="/WEB-INF/jsp/common/header.jsp"%>
<div class="container">
	<h1>Service form</h1>
	<div class="span-12 last">
		<form:form modelAttribute="service" method="post">
			<fieldset>
				<legend>Service fields</legend>
				<p>
					<form:label for="description" path="description"
						cssErrorClass="error">Description</form:label>
					<br />
					<form:input path="description" />
					<form:errors path="description" />
				</p>
				<p>
					<form:label for="price" path="price" cssErrorClass="error">Price</form:label>
					<br />
					<form:input path="price" />
					<form:errors path="price" />
				</p>
				<p>
					<form:label for="serviceDate" path="serviceDate"
						cssErrorClass="error">Service Date</form:label>
					<br />
					<form:input path="serviceDate" />
					<form:errors path="serviceDate" />
				</p>
				<label> Mechanics </label><br />
				<c:forEach var="mechanic" items="${service.mechanics}">
					<form:checkbox path="selectedMechanics" value="${mechanic.id}" />${mechanic.name}
				</c:forEach>
				<br />
				<p>
					<input type="submit" value="Save" />
				</p>
			</fieldset>
		</form:form>
	</div>
	<hr />
	<a href="<c:url value='/car/list'/>">back</a>
</div>

<%@ include file="/WEB-INF/jsp/common/footer.jsp"%>