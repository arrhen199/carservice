<%@ include file="/WEB-INF/jsp/common/includes.jsp"%>
<%@ include file="/WEB-INF/jsp/common/header.jsp"%>

<div class="container">
	<h1>
		Car Service
	</h1>			
	<div class="span-12 last">	
		<ul>					
			<li><a href="<c:url value='/service/mechanic/list'/>">Mechanics List</a></li>			
			<li><a href="<c:url value='/car/list'/>">Cars List</a></li>		
		</ul>
	</div>		
</div>
<%@ include file="/WEB-INF/jsp/common/footer.jsp"%>